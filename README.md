# About

Stable fork of [dockerized-snx](https://github.com/Etn40ff/dockerized-snx) frozen at commit `61e508f`.

# Usage

- Create a directory called `vpn` and move into it
  ```console
  user@host:~$ mkdir vpn
  user@host:~$ cd vpn
  ```

- Clone `snx.docker` from Gitlab
  ```console
  user@host:~/vpn$ git clone https://gitlab.com/resu.framelab/vpn/snx.docker.git snx.docker
  ```

- Create a configuration directory `snx.config`
  ```console
  user@host:~/vpn$ mkdir snx.config
  ```

- Create a configuration file `univaq.fw.exp` inside `snx.config`
  ```console
  user@host:~/vpn$ touch snx.config/univaq.fw.exp
  ```

- Edit the configuration file `snx.config/univaq.fw.exp`
  - **NOTE:** Use your favorite editor, e.g. `vim`
  ```console
  user@host:~/vpn$ vim snx.config/univaq.fw.exp
  ```
  - Enter the following content:
  - **NOTE:** `your_username` and `your_password` should be the same as your **UNIVAQ** account
  ```
  set servername vpn.univaq.it
  set username your_username
  set password your_password
  set build_number 800010003
  ```

- Build the `snx` container image
  ```console
  user@host:~/vpn$ docker build -t snx --network host snx.docker
  ```

- Run the `snx` container
  - **NOTE:** Use the `--network host --privileged` flags to allow it to have the required rights to set up the tunnel and change the routing table
  - **NOTE:** Choose a *friendly_name* for the `snx` container instance
  - **NOTE:** Provide a *read-only* link to the Linux kernel modules, by default it should be `/lib/modules` but it may vary (e.g. on *NixOS* is `/run/current-system/kernel-modules/lib/modules:/lib/modules:ro`)
  ```console
  user@host:~/vpn$ docker run --name friendly_name -d -v $(pwd)/snx.config:/etc/snx/ -v /lib/modules:/lib/modules:ro --network host --privileged snx univaq.fw.exp
  ```

- Inspect the `snx` container logs
    - **NOTE:** It should respond with the message *"Connected."*
	```console
	user@host:~/vpn$ docker logs friendly_name
	Connected.
	```

- Stop and remove the container to disconnect from the VPN
  ```console
  user@host:~/vpn$ docker stop friendly_name
  user@host:~/vpn$ docker rm friendly_name --volumes
  ```
